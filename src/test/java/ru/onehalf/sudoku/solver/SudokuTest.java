package ru.onehalf.sudoku.solver;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Date: 09.01.2017
 *
 * @author OneHalf
 */
public class SudokuTest {

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_thowException_when_createWithIncorrectValues() throws Exception {
        new Sudoku(new Integer[][] {
                {1, 1, 0, 0, 0, 0, 0, 0, 0, },
                {0, 0, 0, 0, 0, 0, 0, 0, 0, },
                {0, 0, 0, 0, 0, 0, 0, 0, 0, },
                {0, 0, 0, 0, 0, 0, 0, 0, 0, },
                {0, 0, 0, 0, 0, 0, 0, 0, 0, },
                {0, 0, 0, 0, 0, 0, 0, 0, 0, },
                {0, 0, 0, 0, 0, 0, 0, 0, 0, },
                {0, 0, 0, 0, 0, 0, 0, 0, 0, },
                {0, 0, 0, 0, 0, 0, 0, 0, 0, },
        });
    }
}