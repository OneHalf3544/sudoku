package ru.onehalf.sudoku.solver;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.testng.annotations.Test;

import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.testng.Assert.*;

/**
 * Date: 08.01.2017
 *
 * @author OneHalf
 */
public class ContainerTest {
    @Test
    public void should_getCorrectAreaByIndex() throws Exception {
        Sudoku sudoku = createSudoku();

        assertEquals(sudoku.getArea(0).getNumber(0), Integer.valueOf(1));
    }

    @Test
    public void should_hasCorrectSize() {
        Sudoku sudoku = createSudoku();

        assertEquals(Iterables.size(sudoku.getArea(0)), 9);
    }

    @Test
    public void should_returnCorrectContent() throws Exception {
        Sudoku sudoku = new Sudoku(new Integer[][]{
                {1, 2, 3, 0, 0, 0, 0, 0, 0,},
                {4, 5, 6, 0, 0, 0, 0, 0, 0,},
                {7, 8, 9, 0, 0, 0, 0, 0, 0,},
                {0, 0, 0, 5, 0, 0, 6, 0, 0,},
                {0, 0, 0, 0, 0, 0, 0, 0, 0,},
                {0, 0, 0, 0, 0, 0, 0, 0, 0,},
                {0, 0, 0, 8, 0, 0, 9, 0, 0,},
                {0, 0, 0, 0, 0, 0, 0, 0, 0,},
                {0, 0, 0, 0, 0, 0, 0, 0, 0,},
        });
        Area area = sudoku.getArea(0);
        for (int i = 0; i < 9; i++) {
            assertEquals(area.getNumber(i), Integer.valueOf(i + 1));
        }
        assertNull(sudoku.getArea(1).getNumber(0));

        assertEquals(sudoku.getRow(0).getNumber(1), Integer.valueOf(2));
        assertEquals(sudoku.getColumn(0).getNumber(1), Integer.valueOf(4));
    }

    @Test
    public void should_beSolved_when_allCellsFilled() throws Exception {
        Sudoku sudoku = createSudoku();
        Area area = sudoku.getArea(0);
        assertFalse(area.isSolved(), "area unexpectedly solved: " + area);

        for (int i = 2; i <= 9; i++) {
            area.setNumber(i - 1, i);
        }
        assertTrue(area.isSolved(), area.stream().map(i -> i + " ").collect(Collectors.joining()));
    }

    private Sudoku createSudoku() {
        return new Sudoku(new Integer[][]{
                {1, null, null, 2, null, null, 3, null, null,},
                {null, null, null, null, null, null, null, null, null,},
                {null, null, null, null, null, null, null, null, null,},
                {4, null, null, 5, null, null, 6, null, null,},
                {null, null, null, null, null, null, null, null, null,},
                {null, null, null, null, null, null, null, null, null,},
                {7, null, null, 8, null, null, 9, null, null,},
                {null, null, null, null, null, null, null, null, null,},
                {null, null, null, null, null, null, null, null, null,},
        });
    }

}