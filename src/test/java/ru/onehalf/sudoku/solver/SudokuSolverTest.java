package ru.onehalf.sudoku.solver;

import com.google.common.collect.ImmutableList;
import org.hamcrest.MatcherAssert;
import org.testng.annotations.Test;
import ru.onehalf.sudoku.solver.Sudoku;
import ru.onehalf.sudoku.solver.SudokuSolver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Date: 08.01.2017
 *
 * @author OneHalf
 */
public class SudokuSolverTest {

    @Test
    public void should_reducePossibleValues_when_hasPair() throws Exception {
        Sudoku sudoku = new Sudoku(new Integer[][]{
                {1, 0, 3, 0, 0, 0, 0, 0, 0,},
                {4, 0, 0, 0, 0, 0, 0, 0, 0,},
                {7, 8, 0, 0, 0, 0, 0, 0, 0,},
                {0, 6, 0, 5, 0, 0, 0, 0, 0,},
                {0, 9, 0, 0, 0, 0, 0, 0, 0,},
                {0, 0, 0, 0, 0, 0, 0, 0, 0,},
                {0, 0, 0, 8, 0, 0, 9, 0, 0,},
                {0, 0, 0, 0, 0, 0, 0, 0, 0,},
                {0, 0, 0, 0, 0, 0, 0, 0, 0,},
        });

        SudokuSolver.solve(sudoku);

        assertThat(sudoku.getPossibleValues(0, 1), is(ImmutableList.of(2, 5)));
        assertThat(sudoku.getPossibleValues(1, 1), is(ImmutableList.of(2, 5)));
        assertThat(sudoku.getPossibleValues(1, 2), is(ImmutableList.of(6, 9)));
        assertThat(sudoku.getPossibleValues(2, 2), is(ImmutableList.of(6, 9)));
    }

    @Test
    public void should_solveMostDifficultPazzle() {

        Sudoku sudoku = new Sudoku(new Integer[][] {
                {8, 0, 0, 0, 0, 0, 0, 0, 0, },
                {0, 0, 3, 6, 0, 0, 0, 0, 0, },
                {0, 7, 0, 0, 9, 0, 2, 0, 0, },
                {0, 5, 0, 0, 0, 7, 0, 0, 0, },
                {0, 0, 0, 0, 4, 5, 7, 0, 0, },
                {0, 0, 0, 1, 0, 0, 0, 3, 0, },
                {0, 0, 1, 0, 0, 0, 0, 6, 8, },
                {0, 0, 8, 5, 0, 0, 0, 1, 0, },
                {0, 9, 0, 0, 0, 0, 4, 0, 0, },
        });

        SudokuSolver.solve(sudoku);

        assertTrue(sudoku.isSolved(), "sudoku is not solved:\n" + sudoku + '\n');
    }

    @Test
    public void should_findSimpleValuesWithRandom() {
        Sudoku sudoku = new Sudoku(new Integer[][] {
                {4, 0, 8, 7, 0, 6, 5, 0, 0, },
                {2, 0, 0, 4, 5, 8, 6, 0, 0, },
                {7, 5, 6, 0, 9, 0, 0, 0, 0, },
                {1, 0, 0, 3, 0, 0, 0, 5, 0, },
                {0, 0, 7, 0, 1, 9, 8, 3, 6, },
                {0, 2, 0, 0, 6, 7, 0, 4, 1, },
                {0, 1, 2, 8, 0, 0, 3, 9, 7, },
                {3, 8, 0, 0, 7, 2, 0, 6, 5, },
                {9, 7, 0, 6, 0, 0, 0, 0, 8, },
        });

        SudokuSolver.solve(sudoku);
        assertEquals(sudoku.getRow(4).getNumber(0), Integer.valueOf(5));
        assertTrue(sudoku.isSolved(), "sudoku is not solved:\n" + sudoku + '\n');
    }

    @Test
    public void should_findVeryHardValues() {
        Sudoku sudoku = new Sudoku(new Integer[][] {
            {0, 0, 4, 2, 8, 0, 6, 0, 0, },
            {6, 0, 0, 0, 0, 0, 8, 0, 1, },
            {7, 0, 0, 6, 0, 0, 0, 0, 0, },
            {0, 0, 3, 0, 7, 0, 0, 1, 0, },
            {0, 0, 0, 9, 0, 6, 0, 0, 0, },
            {0, 4, 0, 0, 1, 0, 7, 0, 0, },
            {0, 0, 0, 0, 0, 9, 0, 0, 8, },
            {5, 0, 6, 0, 0, 0, 0, 0, 7, },
            {0, 0, 9, 0, 3, 2, 5, 0, 0, },
        });

        SudokuSolver.solve(sudoku);
        assertTrue(sudoku.isSolved(), "sudoku is not solved:\n" + sudoku + '\n');
    }

    @Test
    public void should_findHardValues() {
        Sudoku sudoku = new Sudoku(new Integer[][] {
            {0, 8, 0, 0, 7, 0, 0, 0, 0, },
            {0, 6, 9, 0, 0, 0, 1, 0, 0, },
            {0, 0, 7, 9, 2, 0, 0, 0, 0, },
            {3, 1, 0, 0, 8, 0, 6, 0, 0, },
            {0, 0, 0, 7, 0, 1, 0, 0, 0, },
            {0, 0, 2, 0, 4, 0, 0, 1, 5, },
            {0, 0, 0, 0, 6, 5, 7, 0, 0, },
            {0, 0, 6, 0, 0, 0, 3, 2, 0, },
            {0, 0, 0, 0, 3, 0, 0, 4, 0, },
        });

        SudokuSolver.solve(sudoku);
        assertTrue(sudoku.isSolved(), "sudoku is not solved:\n" + sudoku + '\n');
    }

    @Test
    public void should_findSimpleValuesWithoutRandom() {
        Sudoku sudoku = new Sudoku(new Integer[][] {
            {5, 0, 0, 0, 8, 0, 0, 0, 1, },
            {0, 8, 0, 5, 0, 4, 0, 7, 9, },
            {0, 0, 0, 0, 0, 1, 5, 3, 0, },
            {3, 1, 0, 0, 7, 0, 0, 8, 4, },
            {0, 0, 4, 0, 0, 0, 7, 0, 0, },
            {8, 6, 0, 0, 9, 0, 0, 2, 3, },
            {0, 4, 3, 9, 0, 0, 0, 0, 0, },
            {6, 5, 0, 3, 0, 7, 0, 9, 0, },
            {7, 0, 0, 0, 2, 0, 0, 0, 5, },
        });

        SudokuSolver.solve(sudoku);
        assertTrue(sudoku.isSolved(), "sudoku is not solved:\n" + sudoku + '\n');
    }

    @Test
    public void should_reducePossibleValues() {
        Sudoku sudoku = new Sudoku(new Integer[][] {
            {1, 2, 0, 0, 0, 0, 0, 0, 0, },
            {3, 4, 5, 0, 0, 0, 0, 0, 0, },
            {6, 7, 0, 0, 0, 0, 0, 0, 0, },
            {0, 0, 0, 0, 0, 0, 0, 0, 0, },
            {0, 0, 0, 0, 0, 0, 0, 0, 0, },
            {0, 0, 0, 0, 0, 0, 0, 0, 0, },
            {0, 0, 0, 0, 0, 0, 0, 0, 0, },
            {0, 0, 0, 0, 0, 0, 0, 0, 0, },
            {0, 0, 0, 0, 0, 0, 0, 0, 0, },
        });

        SudokuSolver.solve(sudoku);

        assertFalse(sudoku.isSolved());
        assertThat(sudoku.getPossibleValues(0, 2), is(ImmutableList.of(8, 9)));
        assertThat(sudoku.getPossibleValues(8, 2), is(ImmutableList.of(1, 2, 3, 4, 6, 7)));
    }

}