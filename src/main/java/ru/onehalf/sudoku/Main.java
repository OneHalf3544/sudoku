package ru.onehalf.sudoku;

import ru.onehalf.sudoku.solver.Sudoku;
import ru.onehalf.sudoku.solver.SudokuSolver;

/**
 * Date: 08.01.2017
 *
 * @author OneHalf
 */
public class Main {

    public static void main(String[] args) {
        Sudoku sudoku = new Sudoku(new Integer[][] {
                {8,    null, null, null, null, null, null, null, null, },
                {null, null, 3,    6,    null, null, null, null, null, },
                {null, 7,    null, null, 9,    null, 2,    null, null, },
                {null, 5,    null, null, null, 7,    null, null, null, },
                {null, null, null, null, 4,    5,    7,    null, null, },
                {null, null, null, 1,    null, null, null, 3,    null, },
                {null, null, 1,    null, null, null, null, 6,    8, },
                {null, null, 8,    5,    null, null, null, 1,    null, },
                {null, 9,    null, null, null, null, 4,    null, null, },
        });

        SudokuSolver.solve(sudoku);

        System.out.println(sudoku);
    }
}
