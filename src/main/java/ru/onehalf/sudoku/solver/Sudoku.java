package ru.onehalf.sudoku.solver;

import java.util.*;
import java.util.stream.IntStream;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.stream.Collectors.joining;

/**
 * Date: 08.01.2017
 *
 * @author OneHalf
 */
public class Sudoku {

    public static final int SUDOKU_SIZE = 9;

    private final Integer[][] numbers;
    private final List<List<List<Integer>>> possibleValues;

    public Sudoku(Integer[][] numbers ) {
        possibleValues = createAllPossibleValues();

        checkArgument(numbers.length == SUDOKU_SIZE);
        for (Integer[] rows : numbers) {
            checkArgument(rows.length == SUDOKU_SIZE);
            for (int i = 0; i < SUDOKU_SIZE; i++) {
                if (Objects.equals(rows[i], 0)) {
                    rows[i] = null;
                }
                if (rows[i] != null) {
                    checkNumberRange(rows[i]);
                }
            }
        }
        this.numbers = numbers;

        for (int i = 0; i < SUDOKU_SIZE; i++) {
            boolean correct = getRow(i).isCorrect()
                    && getColumn(i).isCorrect()
                    && getArea(i).isCorrect();
            if (!correct) {
                throw new IllegalArgumentException();
            }
        }
    }

    List<Integer> getPossibleValues(int row, int column) {
        checkIndexRange(row);
        checkIndexRange(column);
        return possibleValues.get(row).get(column);
    }

    private List<List<List<Integer>>> createAllPossibleValues() {
        List<List<List<Integer>>> result = new ArrayList<>(SUDOKU_SIZE);
        for (int i = 0; i < SUDOKU_SIZE; i++) {
            List<List<Integer>> row = new ArrayList<>(SUDOKU_SIZE);
            for (int j = 0; j < SUDOKU_SIZE; j++) {
                List<Integer> values = new ArrayList<>(SUDOKU_SIZE);
                for (int k = 1; k <= SUDOKU_SIZE; k++) {
                    values.add(k);
                }
                row.add(values);
            }
            result.add(row);
        }
        return result;
    }

    static int checkIndexRange(int index) {
        if (index < 0 || index > SUDOKU_SIZE - 1) {
            throw new IllegalArgumentException("wrong index: " + index);
        }
        return index;
    }

    static int checkNumberRange(int number) {
        if (number < 1 || number > SUDOKU_SIZE) {
            throw new IllegalArgumentException("wrong number: " + number);
        }
        return number;
    }

    public Row getRow(int rowIndex) {
        return new Row(this, checkIndexRange(rowIndex));
    }

    public Column getColumn(int columnIndex) {
        return new Column(this, columnIndex);
    }

    public Area getArea(int areaIndex) {
        return new Area(this, areaIndex);
    }

    Integer[][] getContent() {
        return numbers;
    }

    public boolean isSolved() {
        for (int i = 0; i < SUDOKU_SIZE; i++) {
            if (getRow(i).isSolved() && getColumn(i).isSolved() && getArea(i).isSolved()) {
                continue;
            }
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return IntStream.range(0, SUDOKU_SIZE)
                .mapToObj(i -> getRow(i).toString())
                .collect(joining("\n"));
    }
}
