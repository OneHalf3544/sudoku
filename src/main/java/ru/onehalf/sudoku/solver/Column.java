package ru.onehalf.sudoku.solver;

import java.util.List;

/**
 * Date: 09.01.2017
 *
 * @author OneHalf
 */
class Column extends Container {

    private final int columnIndex;

    public Column(Sudoku sudoku, int columnIndex) {
        super(sudoku);
        this.columnIndex = columnIndex;
    }

    @Override
    public Integer getNumber(int index) {
        Sudoku.checkIndexRange(index);
        return sudoku.getContent()[index][columnIndex];
    }

    @Override
    public void setNumber(int index, Integer number) {
        Sudoku.checkIndexRange(index);
        Sudoku.checkNumberRange(number);
        sudoku.getContent()[index][columnIndex] = number;
    }

    @Override
    public List<Integer> getPossibleValues(int index) {
        Sudoku.checkIndexRange(index);
        return sudoku.getPossibleValues(index, columnIndex);
    }
}
