package ru.onehalf.sudoku.solver;

import java.util.List;

/**
 * Date: 09.01.2017
 *
 * @author OneHalf
 */
class Row extends Container {

    private final int rowIndex;

    public Row(Sudoku sudoku, int rowIndex) {
        super(sudoku);
        this.rowIndex = rowIndex;
    }

    @Override
    public Integer getNumber(int index) {
        Sudoku.checkIndexRange(index);
        return sudoku.getContent()[rowIndex][index];
    }

    @Override
    public void setNumber(int index, Integer number) {
        Sudoku.checkNumberRange(number);
        Sudoku.checkIndexRange(index);
        sudoku.getContent()[rowIndex][index] = number;
    }

    @Override
    public List<Integer> getPossibleValues(int index) {
        Sudoku.checkIndexRange(index);
        return sudoku.getPossibleValues(rowIndex, index);
    }
}
