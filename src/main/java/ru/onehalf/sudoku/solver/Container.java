package ru.onehalf.sudoku.solver;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Date: 08.01.2017
 *
 * @author OneHalf
 */
public abstract class Container implements Iterable<Integer> {

    protected final Sudoku sudoku;

    public Container(Sudoku sudoku) {
        this.sudoku = sudoku;
    }

    public abstract Integer getNumber(int index);

    public abstract void setNumber(int index, Integer number);

    public abstract List<Integer> getPossibleValues(int index);

    public int solvedCells() {
        return (int) stream().filter(Objects::nonNull).count();
    }

    public boolean isSolved() {
        return stream()
                .filter(Objects::nonNull)
                .distinct()
                .count() == Sudoku.SUDOKU_SIZE;
    }

    Stream<Integer> stream() {
        return StreamSupport.stream(this.spliterator(), false);
    }

    public boolean isCorrect() {
        return stream().filter(Objects::nonNull).count() ==
                stream().filter(Objects::nonNull).distinct().count();
    }

    @Override
    public String toString() {
        return stream()
                .map(i -> (i == null ? "_" : i) + " ")
                        .collect(Collectors.joining());
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            int index = 0;

            @Override
            public boolean hasNext() {
                return index < Sudoku.SUDOKU_SIZE;
            }

            @Override
            public Integer next() {
                return getNumber(index++);
            }
        };
    }

}
