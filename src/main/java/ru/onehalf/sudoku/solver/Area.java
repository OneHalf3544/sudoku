package ru.onehalf.sudoku.solver;

import java.util.List;

/**
 * Date: 09.01.2017
 *
 * @author OneHalf
 */
class Area extends Container {

    private final int areaId;

    public Area(Sudoku sudoku, int areaId) {
        super(sudoku);
        this.areaId = areaId;
    }

    @Override
    public Integer getNumber(int index) {
        Sudoku.checkIndexRange(index);
        return sudoku.getRow(getRowIndexForArea(index)).getNumber(getColumnIndexForArea(index));
    }

    @Override
    public void setNumber(int index, Integer number) {
        Sudoku.checkIndexRange(index);
        Sudoku.checkNumberRange(number);
        sudoku.getRow(getRowIndexForArea(index)).setNumber(getColumnIndexForArea(index), number);
    }

    @Override
    public List<Integer> getPossibleValues(int index) {
        Sudoku.checkIndexRange(index);
        return sudoku.getPossibleValues(getRowIndexForArea(index), getColumnIndexForArea(index));
    }

    private int getColumnIndexForArea(int index) {
        return 3 * (areaId % 3) + index % 3;
    }

    private int getRowIndexForArea(int index) {
        return 3 * (areaId / 3) + index / 3;
    }
}
