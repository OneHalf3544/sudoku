package ru.onehalf.sudoku.solver;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static ru.onehalf.sudoku.solver.Sudoku.SUDOKU_SIZE;

/**
 * Date: 08.01.2017
 *
 * @author OneHalf
 */
public class SudokuSolver {

    public static void solve(Sudoku sudoku) {
        int oldPossibleValuesCount;
        int newPossibleValuesCount = SUDOKU_SIZE * SUDOKU_SIZE * SUDOKU_SIZE;
        do {
            oldPossibleValuesCount = newPossibleValuesCount;
            for (int i = 0; i < SUDOKU_SIZE; i++) {
                updatePossibleValuesInContainer(sudoku.getRow(i));
                updatePossibleValuesInContainer(sudoku.getColumn(i));
                updatePossibleValuesInContainer(sudoku.getArea(i));
            }
            newPossibleValuesCount = replaceOnePossibleValues(sudoku);

        } while (newPossibleValuesCount != oldPossibleValuesCount);
    }

    /**
     * Выкидывает числа, корорые недопустимы в ячейках переданной области.
     *
     * @param container Область, в которой ищем "невозможные" значения
     */
    private static void updatePossibleValuesInContainer(Container container) {
        Set<Integer> usedNumbersInRow = container.stream()
                .filter(Objects::nonNull)
                .collect(toSet());

        for (int j = 0; j < SUDOKU_SIZE; j++) {
            if (container.getNumber(j) != null) {
                // Если число уже проставлено, удаляем все остальные возможные значения.
                container.getPossibleValues(j).clear();
                continue;
            }
            // Удаляем уже проставленные числа из значений других ячеек.
            container.getPossibleValues(j).removeAll(usedNumbersInRow);
        }

        // Удаляем лишние варианты, их ходя из принципа
        // "это число будет тут или тут, но в других ячейках точно не появится"

        int unsolvedCells = SUDOKU_SIZE - container.solvedCells();

        for (int i = 0; i < SUDOKU_SIZE - 1; i++) {
            // Если в какой-то ячейке возможны все еще не открытые значения, то нет смысла ее анализировать.
            if (container.getPossibleValues(i).size() < unsolvedCells) {
                List<Integer> equalsValuesIndex = new ArrayList<>();
                equalsValuesIndex.add(i);
                for (int j = i + 1; j < SUDOKU_SIZE; j++) {
                    // В остатке области ищем ячейки с такими же возможными значениями.
                    // Для удачного исхода, число таких ячеек должно быть равно числу возможных значений.
                    if (container.getPossibleValues(i).equals(container.getPossibleValues(j))) {
                        equalsValuesIndex.add(j);
                    }
                }
                if (equalsValuesIndex.size() == container.getPossibleValues(i).size()) {
                    for (int j = 0; j < SUDOKU_SIZE; j++) {
                        if (!equalsValuesIndex.contains(j)) {
                            container.getPossibleValues(j).removeAll(container.getPossibleValues(i));
                        }
                    }
                }
            }
        }
    }

    private static int replaceOnePossibleValues(Sudoku sudoku) {
        int newPossibleValuesCount = 0;
        for (int row = 0; row < SUDOKU_SIZE; row++) {
            for (int column = 0; column < SUDOKU_SIZE; column++) {
                List<Integer> possibleValues = sudoku.getPossibleValues(row, column);
                if (possibleValues.size() == 1) {
                    sudoku.getContent()[row][column] = possibleValues.get(0);
                    possibleValues.clear();
                }
                newPossibleValuesCount += possibleValues.size();
            }
        }
        return newPossibleValuesCount;
    }
}
